Publications
============

The following publications have been made possible with cRacklet:

- `Barras, F., Kammer, D. S., Geubelle, P. H., Molinari, J.-F. (2014) A study of frictional contact in dynamic fracture along bimaterial interfaces. International Journal of Fracture 189(2), 149–162 <https://doi.org/10.1007/s10704-014-9967-z>`_

- `Barras, F., Geubelle, P. H., Molinari, J.-F. (2017) Interplay between Process Zone and Material Heterogeneities for Dynamic Cracks. Physical Review Letters 119(14) <https://doi.org/10.1103/PhysRevLett.119.144101>`_

- `Barras, F., Carpaij, R., Geubelle, P. H., & Molinari, J.-F. (2018). Supershear bursts in the propagation of a tensile crack in linear elastic material. Physical Review E, 98(6), 063002. <https://doi.org/10.1103/PhysRevE.98.063002>`_

- `Brener, E. A., Aldam, M., Barras, F., Molinari, J.-F., & Bouchbinder, E. (2018). Unstable Slip Pulses and Earthquake Nucleation as a Nonequilibrium First-Order Phase Transition. Physical Review Letters, 121(23), 234302. <https://doi.org/10.1103/PhysRevLett.121.234302>`_

- `Barras, F., Aldam, M., Roch, T., Brener, E. A., Bouchbinder, E., & Molinari, J.-F. (2019). Emergence of Cracklike Behavior of Frictional Rupture: The Origin of Stress Drops. Physical Review X, 9(4), 041043. <https://doi.org/10.1103/PhysRevX.9.041043>`_

- `Barras, F., Aldam, M., Roch, T., Brener, E. A., Bouchbinder, E., & Molinari, J.-F. (2020). The emergence of crack-like behavior of frictional rupture: Edge singularity and energy balance. Earth and Planetary Science Letters, 531, 115978. <https://doi.org/10.1016/j.epsl.2019.115978>`_

- `Fekak, F., Barras, F., Dubois, A., Spielmann, D., Bonamy, D., Geubelle, P. H., & Molinari J. F. (2020). Crack front waves: A 3D dynamic response to a local perturbation of tensile and shear cracks. Journal of the Mechanics and Physics of Solids, 135, 103806. <https://doi.org/10.1016/j.jmps.2019.103806>`_

- `Rezakhani, R., Barras, F., Brun, M., & Molinari, J.-F. (2020). Finite element modeling of dynamic frictional rupture with rate and state friction. Journal of the Mechanics and Physics of Solids, 141, 103967. <https://doi.org/10.1016/j.jmps.2020.103967>`_

- `Brener, E. A., & Bouchbinder, E. (2021). Unconventional singularities and energy balance in frictional rupture. Nature Communications, 12(1), 2585. <https://doi.org/10.1038/s41467-021-22806-9>`_

- `Lebihain, M., Roch, T., Violay, M., & Molinari, J.-F. (2021). Eartquake Nucleation Along Fault With Heterogeneous Weakening Rate. Geophysical Research Letters, 48 e2021GL094901. <https://doi.org/10.1029/2021GL094901>`_

- `Roch, T., Brener, E. A., Molinari, J.-F., & Bouchbinder, E. (2022). Velocity-driven frictional sliding: Coarsening and steady-state pulse trains. Journal of the Mechanics and Physics of Solids, 158, 104607. <https://doi.org/10.1016/j.jmps.2021.104607>`_
