API Reference
=============

Python API
----------

.. autoclass:: py11_cracklet.SpectralModel
   :members:
   :show-inheritance:

   .. automethod:: __init__

.. autoclass:: py11_cracklet.SimulationDriver
   :members:
   :show-inheritance:

   .. automethod:: __init__
      
cracklet Interface Laws
^^^^^^^^^^^^^^^^^^^^^^^

.. autoclass:: py11_cracklet.CohesiveLaw
   :members:
   :show-inheritance:

   .. automethod:: __init__
      
.. autoclass:: py11_cracklet.InterfacerLinearCoupledCohesive
   :members:
   :show-inheritance:
      
.. autoclass:: py11_cracklet.RateAndStateLaw
   :members:
   :show-inheritance:
      
cracklet Interfacer
^^^^^^^^^^^^^^^^^^^
      
.. autoclass:: py11_cracklet.InterfacerRateAndState
   :members:
   :show-inheritance:

.. autoclass:: py11_cracklet.InterfacerRegularizedRateAndState
   :members:
   :show-inheritance:
      
.. autoclass:: py11_cracklet.InterfacerWeakeningRateAndState
   :members:
   :show-inheritance:
      
cracklet Contact Laws
^^^^^^^^^^^^^^^^^^^^^
      
.. autoclass:: py11_cracklet.CoulombLaw
   :members:
   :show-inheritance:

.. autoclass:: py11_cracklet.RegularizedCoulombLaw
   :members:
   :show-inheritance:

cracklet Dumper and Data Management
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. autoclass:: py11_cracklet.DataRegister
   :members:
   :show-inheritance:

.. autoclass:: py11_cracklet.DataDumper
   :members:
   :show-inheritance:
